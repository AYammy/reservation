/**It is a root module. It helps to incorporate the feature module into the app.
 * Other modules can get to the new feature, only if it is imported here 
 * and added to the imports array */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // <-- NgModel
import { MatCardModule, MatButtonModule } from '@angular/material';
import { AppComponent } from './app.component';
import { DesksComponent } from './components/main-content/desks/desks.component';
import { AppRoutingModule } from './app-routing.module';
import { PlanComponent } from './components/main-content/plan/plan.component';
import { HttpClientModule } from '@angular/common/http';
import { DeskSearchComponent } from './components/main-content/desk-search/desk-search.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { ToolbarComponent } from './components/layout/toolbar/toolbar.component';
import { MatToolbarModule } from '@angular/material/toolbar';
// see all icons here https://material.io/resources/icons/?style=baseline
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { SidebarComponent } from './components/side-content/sidebar/sidebar.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { TabsComponent } from './components/main-content/tabs/tabs.component';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { TableComponent } from './components/main-content/table/table.component';
import { MatTableModule } from '@angular/material/table';
import { LoginComponent } from './components/login/login.component';
import { MatProgressSpinnerModule } from '@angular/material';

//@NgModule is a decorator. Throught export property it makes some components public.
@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatTooltipModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatTabsModule,
    MatFormFieldModule,
    MatSelectModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    MatDialogModule,
    MatTableModule,
    MatCardModule,
    MatButtonModule,
    MatProgressSpinnerModule,
    MatCardModule,

  ],
  declarations: [
    AppComponent,
    FooterComponent,
    DesksComponent,
    PlanComponent,
    DeskSearchComponent,
    FooterComponent,
    ToolbarComponent,
    SidebarComponent,
    TabsComponent,
    TableComponent,
    LoginComponent,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
