import { Location } from './location';

export class Sector {
  id: number;
  sectorNo: string;
  floor: string;
  location: Location;
  locationId: number;
}
