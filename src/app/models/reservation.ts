import { User } from './user';
import { Desk } from './desk';

export class Reservation {
  id: number;
  user: User;
  desk: Desk;
  from: string;
  to: string;
}
