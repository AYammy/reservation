import { Sector } from './sector';
import { Group } from './group';

export class Workspace {
    id: number;
    sector: Sector;
    group: Group;
}
