import { Group } from './group';

export class User {
  id: number;
  firstname: string;
  group: Group;
}
