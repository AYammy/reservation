import { Workspace } from './workspace';
import { User } from './user';

export class Desk {
  id: number;
  AP_NR: string;
  deskOwner: User;
  workspace: Workspace;
}
