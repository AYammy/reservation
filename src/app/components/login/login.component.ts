import { Component } from '@angular/core';
import { AuthGuardService } from '../../guards/auth-guard.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  constructor(private authGuard: AuthGuardService, private router: Router) { }
  username: string;
  password: string;


  login() {
    console.debug('I am in LoginComponent.login.');
    this.authGuard.login(this.username, this.password);
    console.debug('AuthGuardService.login is called.');
    this.router.navigate(['/desks']);
  }
}
