import { Component, OnInit } from '@angular/core';
import { Desk } from '../../../models/desk';
import { DeskService } from '../../../services/desk.service';
import { Reservation } from '../../../models/reservation';
import { ReservationService } from '../../../services/reservation.service';
import { SubjectsService } from 'src/app/services/subjects.service';
import { Sector } from 'src/app/models/sector';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})

export class TableComponent implements OnInit{
  desks: Desk[];
  reservations: Reservation[];
  currentDate: Date;
  currentSector: Sector;

  constructor(
    private deskService: DeskService,
    private reservationService: ReservationService,
    private subjectService: SubjectsService
  ) { }

  ngOnInit() {
    this.currentDate = new Date();
    this.subjectService.getSectorsSubject().subscribe(sector => this.filterBySector(sector));
    this.subjectService.getDateSubject().subscribe(date => this.currentDate = date);
  }

private filterBySector(sector: Sector) {
    console.debug(sector.locationId);
    this.reservationService.getReservationsByLocationId(sector.location.id).subscribe(r => this.reservations = r);
    this.deskService.getDesksByLocationId(sector.location.id)
      .subscribe(locationDesks => {
        this.desks = locationDesks.filter(desk => desk.workspace.sector.id == sector.id);
        console.debug(this.desks)
      });
  }

  isDeskReserved(deskId: number, date:Date): boolean {
    if (this.reservations) {
      let relevantReservations = this.reservations.filter(r => this.isRelevantReservation(r, deskId, date));
      if (relevantReservations && relevantReservations.length) {
        return true;
      }
    }
    return false;
  }

  getReservation(deskId: number, date: Date) {
    return this.reservations.find(r => this.isRelevantReservation(r, deskId, date));
  }

  isRelevantReservation(reservation: Reservation, deskId: number, date: Date): boolean {
    if (reservation.desk.id == deskId) {
      let reservationTo = new Date(reservation.to);
      reservationTo.setHours(0, 0, 0, 0);
      let reservationFrom = new Date(reservation.from);
      reservationFrom.setHours(0, 0, 0, 0);
      return reservationFrom <= date
        && date <= reservationTo
    }
    return false;
  }

  book(desk: Desk, date: Date) {
    console.log('Booking is preparing to be sent.');
    this.reservationService.postReservation(desk, date)
    .subscribe(x => this.filterBySector(this.currentSector));
    console.log('Booking has been sent.');
    
  }

  addDays(d: Date, days: number): Date {
    let result = new Date();
    result.setHours(0, 0, 0, 0);
    d.setHours(0, 0, 0, 0);
    result.setDate(d.getDate() + days);
    return result;
  }
  displayedColumns: string[] = ['AP_NR','date1', 'date2', 'date3', 'date4', 'date5'];

}
