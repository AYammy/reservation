import { Component, OnInit } from '@angular/core';

import { Desk } from '../../../models/desk';
import { DeskService } from '../../../services/desk.service';

export interface Tile {
  color: string;
  cols: number;
  rows: number;
  desk: string;
  place: string;
  name:string;
  placeholder: string;
  reserved: boolean;
  my_reservation: boolean;
}


@Component({
  selector: 'app-plan',
  templateUrl: './plan.component.html',
  styleUrls: ['./plan.component.css']
})
export class PlanComponent implements OnInit {
  desks: Desk[] = [];

  tiles: Tile[] = [
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: '', desk: '', cols: 3, rows: 2, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: '', desk: '', cols: 1, rows: 14, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: 'down', desk: '1.72', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: 'down', desk: '1.73', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: '', desk: '', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Carmen', reserved: true, place: 'up', desk: '1.74', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: '', desk: '', cols: 3, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Igor', reserved: true, place: 'down', desk: '1.75', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Ürün', reserved: true, place: 'down', desk: '1.76', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Eva', reserved: true, place: 'down', desk: '1.83', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Sofie', reserved: true, place: 'down', desk: '1.84', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Matthias', reserved: true, place: 'down', desk: '1.85', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Leo', reserved: true, place: 'up', desk: '1.77', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Andy', reserved: true, place: 'up', desk: '1.78', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Tobias', reserved: true, place: 'up', desk: '1.86', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Thomas', reserved: true, place: 'up', desk: '1.87', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Jasmin', reserved: true, place: 'up', desk: '1.88', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: '', desk: '', cols: 2, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: '', desk: '', cols: 3, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Sergej', reserved: true, place: 'down', desk: '1.79', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: 'down', desk: '1.80', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: 'down', desk: '1.89', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Elias', reserved: true, place: 'down', desk: '1.90', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Markus', reserved: true, place: 'down', desk: '1.91', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Mark', reserved: true, place: 'up', desk: '1.81', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Herman', reserved: true, place: 'up', desk: '1.82', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Barbara', reserved: true, place: 'up', desk: '1.92', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: 'up', desk: '1.93', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: 'up', desk: '1.94', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: '', desk: '', cols: 2, rows: 2, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: '', desk: '', cols: 3, rows: 6, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Anna', reserved: true, place: 'down', desk: '1.95', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Lena', reserved: true, place: 'down', desk: '1.96', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Tanja', reserved: true, place: 'up', desk: '1.97', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Marie', reserved: true, place: 'up', desk: '1.98', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'', reserved: false, place: '', desk: '', cols: 2, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Viktor', reserved: true, place: 'down', desk: '1.99', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Georg', reserved: true, place: 'down', desk: '1.100', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Wolfgang', reserved: true, place: 'up', desk: '1.101', cols: 1, rows: 1, color: 'white' },
    {  my_reservation:false, placeholder: '_',  name:'Tobi', reserved: true, place: 'up', desk: '1.102', cols: 1, rows: 1, color: 'white' },
  ];

  constructor(private deskService: DeskService) { }

  ngOnInit() { }

  onClick(item){
    if (item.name =='') 
      {console.log(item.name); item.name ='Azida'; }
    else if (item.name =='Azida')
      {console.log(item.name);item.name ='';} 

    item.my_reservation = !item.my_reservation;
  }

}
