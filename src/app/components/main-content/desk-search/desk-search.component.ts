import { Component, OnInit } from '@angular/core';

import { Location } from '../../../models/location';
import { LocationService } from '../../../services/location.service';

import { Group } from '../../../models/group';
import { Sector } from '../../../models/sector';
import { SectorService } from '../../../services/sector.service';

import { Desk } from '../../../models/desk';
import { DeskService } from '../../../services/desk.service';
import { Observable, of, Subject } from 'rxjs';
import { MatSelectChange, MatDatepicker } from '@angular/material';
import { SubjectsService } from 'src/app/services/subjects.service';
import { FormControl } from '@angular/forms';

/** FORDATE import {Datepicker} from '../../../models/date';*/


@Component({
  selector: 'app-desk-search',
  templateUrl: './desk-search.component.html',
  styleUrls: ['./desk-search.component.css']
})

export class DeskSearchComponent implements OnInit {

  selectedLocation: Location = { id: 1, country: "germany", city: "Bad Homburg" };
  selectedSector: Sector = {
    id: 1,
    floor: "2",
    sectorNo: "6",
    location: {
      id: 1,
      country: "germany",
      city: "Bad Homburg"
    },
    locationId: 1
  };

  locations: Location[];
  sectors: Sector[];
  picker: Observable<MatDatepicker<Date>>;
  startDate = new Date();
  endD = new Date();
  endDate = this.endD.setMonth(this.endD.getMonth() + 1);
  date = new FormControl(new Date());
  locationFormControl = new FormControl();
  sectorFormControl = new FormControl();

  constructor(
    locationService: LocationService,
    private sectorService: SectorService,
    private subjectsService: SubjectsService) {
    locationService.getLocations().subscribe(l => {
      this.locations = l;
      this.locationFormControl.setValue(this.locations[0]);
      this.subjectsService.getLocationSubject().next(this.locationFormControl.value);
    });
  }

  ngOnInit() {
    this.subjectsService.getDateSubject().next(this.startDate);
    this.subjectsService.getLocationSubject().subscribe(l => this.chaneActualLocation(l));
    //this.subjectsService.getSectorsSubject().subscribe(s => this.changeActualSector(s));
    this.subjectsService.getSectorsSubject().next(this.selectedSector);
  }

  /**
   * Event handling for location selection.
   * 
   * @param event Selection event with selected value.
   */
  locationChanged(event: MatSelectChange) {
    var currentLocation: Location = event.value;
    this.chaneActualLocation(currentLocation);
  }

  chaneActualLocation(location: Location) {

    this.sectorService.getSectorsByLocation(location.id).subscribe(s => {
      this.sectors = s;
      this.sectorFormControl.setValue(this.sectors[0]);
      this.changeActualSector(this.sectorFormControl.value);
    });
  }

  sectorChanged(event: MatSelectChange) {
    var currentSector: Sector = event.value;
    this.changeActualSector(currentSector);
  }

  changeActualSector(currentSector: Sector) {
    console.log(currentSector);
    this.subjectsService.getSectorsSubject().next(currentSector);
  }

  dateChanged(event: MatSelectChange) {
    var currentDate: Date = event.value;
    console.log(currentDate);
    this.subjectsService.getDateSubject().next(currentDate);
  }


}