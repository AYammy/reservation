import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../../services/customer.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent {

  constructor(private customerService: CustomerService, private router: Router) { }

  logout() {
    this.customerService.removeToken();
    this.router.navigate(['/login']);
  }

  isLoggedIn() {
    return this.customerService.isLogged();
  }

}
