import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
    createDb() {

        const locations = [
            {
                id: 1,
                country: "germany",
                city: "Bad Homburg",
                street: "Marienbader Pl.",
                buildingNo: "1",
                postCode: "61348",
            },
            {
                id: 2,
                country: "germany",
                city: "Nürnberg",
                street: "Nelson-Mandela-Platz",
                buildingNo: "18",
                postCode: "90459",

            },
        ];

        const sectors = [
            {
                id: 1,
                floor: "2",
                sectorNo: "6",
                location: {
                    id: 1,
                    country: "germany",
                    city: "Bad Homburg",
                    street: "Marienbader Pl.",
                    buildingNo: "1",
                    postCode: "61348",
                    links: []
                }
            },
            {
                id: 2,
                floor: "1",
                sectorNo: "6",
                location: {
                    id: 1,
                    country: "germany",
                    city: "Bad Homburg",
                    street: "Marienbader Pl.",
                    buildingNo: "1",
                    postCode: "61348",
                    links: []
                }
            },
            {
                id: 3,
                floor: "1",
                sectorNo: "5",
                location: {
                    id: 1,
                    country: "germany",
                    city: "Bad Homburg",
                    street: "Marienbader Pl.",
                    buildingNo: "1",
                    postCode: "61348",
                    links: []
                }
            },
        ]

        const desks = [
            {
                id: 1,
                AP_NR: "1.91",
                hardware: {
                    id: 1,
                    hasDockingStation: true,
                    dockingStationType: "HP_NEU",
                    hasMouse: true,
                    hasTelephoneConnection: true,
                    hasKeyboard: true,
                    hasMonitor: true,
                    hasLANCable: true
                },
                workspace: {
                    id: 1,
                    sector: {
                        id: 1,
                        floor: "2",
                        sectorNo: "6",
                        location: {
                            id: 1,
                            country: "germany",
                            city: "Bad Homburg",
                            street: "Marienbader Pl.",
                            buildingNo: "1",
                            postCode: "61348",
                            links: []
                        }
                    },
                    group: {
                        id: 1,
                        groupName: "Output & Archiving",
                        groupNumber: "44",
                        groupManager: "Maik Dittrich"
                    }
                },
                deskOwner: "Nicole",
                links: [
                    {
                        "link": "https://localhost/deskable/webapi/desks/1",
                        "rel": "self"
                    }
                ]
            },
            {
                id: 2,
                AP_NR: "1.90",
                hardware: {
                    id: 1,
                    hasDockingStation: true,
                    dockingStationType: "HP_NEU",
                    hasMouse: true,
                    hasTelephoneConnection: true,
                    hasKeyboard: true,
                    hasMonitor: true,
                    hasLANCable: true
                },
                workspace: {
                    id: 1,
                    sector: {
                        id: 1,
                        floor: "2",
                        sectorNo: "6",
                        location: {
                            id: 1,
                            country: "germany",
                            city: "Bad Homburg",
                            street: "Marienbader Pl.",
                            buildingNo: "1",
                            postCode: "61348",
                            links: []
                        }
                    },
                    group: {
                        id: 1,
                        groupName: "Output & Archiving",
                        groupNumber: "44",
                        groupManager: "Maik Dittrich"
                    }
                },
                deskOwner: "Karin",
                links: [
                    {
                        "link": "https://localhost/deskable/webapi/desks/2",
                        "rel": "self"
                    }
                ]
            },
            {
                id: 3,
                AP_NR: "1.89",
                hardware: {
                    id: 1,
                    hasDockingStation: true,
                    dockingStationType: "HP_NEU",
                    hasMouse: true,
                    hasTelephoneConnection: true,
                    hasKeyboard: true,
                    hasMonitor: true,
                    hasLANCable: true
                },
                workspace: {
                    id: 1,
                    sector: {
                        id: 1,
                        floor: "2",
                        sectorNo: "6",
                        location: {
                            id: 1,
                            country: "germany",
                            city: "Bad Homburg",
                            street: "Marienbader Pl.",
                            buildingNo: "1",
                            postCode: "61348",
                            links: []
                        }
                    },
                    group: {
                        id: 1,
                        groupName: "Output & Archiving",
                        groupNumber: "44",
                        groupManager: "Maik Dittrich"
                    }
                },
                deskOwner: "Andreas",
                links: [
                    {
                        "link": "https://localhost/deskable/webapi/desks/3",
                        "rel": "self"
                    }
                ]
            }
        ]

        const reservations = [
            {
                id: 5,
                user: {
                    id: 1,
                    firstname: " Emre",
                    lastname: "Cumbus",
                    email: "Emre.Cumbus@firstdata.com",
                    title: "Technical Trainer",
                    credentials: {
                        username: "f70e9z0",
                        password: "43eb567ee8095b25bba56292ef09c6e039493c87f88baf84890140058beb116c",
                        authority: "POSTRESERVATION",
                        links: []
                    },
                    group: {
                        id: 2,
                        groupName: " Output & Archiving",
                        groupNumber: "FDD414",
                        groupManager: "CN=Dittrich, Maik,CN=Users,DC=gzs,DC=de"
                    },
                    links: []
                },
                desk: {
                    id: 2,
                    AP_NR: "1.90",
                    hardware: {
                        id: 1,
                        hasDockingStation: true,
                        dockingStationType: "HP_NEU",
                        hasMouse: true,
                        hasTelephoneConnection: true,
                        hasKeyboard: true,
                        hasMonitor: true,
                        hasLANCable: true
                    },
                    workspace: {
                        id: 1,
                        sector: {
                            id: 1,
                            floor: "2",
                            sectorNo: "6",
                            location: {
                                id: 1,
                                country: "germany",
                                city: "Bad Homburg",
                                street: "Marienbader Pl.",
                                buildingNo: "1",
                                postCode: "61348",
                                links: []
                            }
                        },
                        group: {
                            id: 1,
                            groupName: "Output & Archiving",
                            groupNumber: "44",
                            groupManager: "Maik Dittrich"
                        }
                    },
                    deskOwner: "Karin",
                    links: []
                },
                from: "2019-09-29",
                to: "2019-09-30",
                links: []
            },
            {
                id: 6,
                user: {
                    id: 2,
                    firstname: "Azida",
                    lastname: "Yambaeva",
                    email: "azida.yambaeva@firstdata.com",
                    title: "Trainee",
                    credentials: {
                        username: "azida",
                        password: "1234",
                        authority: "ADMIN",
                        links: []
                    },
                    lastLogin: "2019-09-24",
                    group: {
                        id: 1,
                        groupName: "Output & Archiving",
                        groupNumber: "44",
                        groupManager: "Maik Dittrich"
                    },
                    links: []
                },
                desk: {
                    id: 2,
                    AP_NR: "1.90",
                    hardware: {
                        id: 1,
                        hasDockingStation: true,
                        dockingStationType: "HP_NEU",
                        hasMouse: true,
                        hasTelephoneConnection: true,
                        hasKeyboard: true,
                        hasMonitor: true,
                        hasLANCable: true
                    },
                    workspace: {
                        id: 1,
                        sector: {
                            id: 1,
                            floor: "2",
                            sectorNo: "6",
                            location: {
                                id: 1,
                                country: "germany",
                                city: "Bad Homburg",
                                street: "Marienbader Pl.",
                                buildingNo: "1",
                                postCode: "61348",
                                links: []
                            }
                        },
                        group: {
                            id: 1,
                            groupName: "Output & Archiving",
                            groupNumber: "44",
                            groupManager: "Maik Dittrich"
                        }
                    },
                    deskOwner: "Karin",
                    links: []
                },
                from: "2019-09-27",
                to: "2019-09-28",
                links: []
            }
        ];

        return { desks, locations, reservations, sectors };
    }
}

  // Overrides the genId method to ensure that a desk always has an id.
  // If the desks array is empty,
  // the method below returns the initial number (11).
  // if the desks array is not empty, the method below returns the highest
  // deks id + 1.
  // genId(desks: Desk[]): number {
  // return desks.length > 0 ? Math.max(...desks.map(desk => desk.id)) + 1 : 11;
  // }

