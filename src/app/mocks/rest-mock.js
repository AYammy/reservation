const express = require('express');
const server = express();
var cors = require('cors')

server.use(cors())
server.use(express.json());

const port = 4000;

const locations = [{
    id: 1,
    country: "germany",
    city: "Bad Homburg",
    street: "Marienbader Pl.",
    buildingNo: "1",
    postCode: "61348",
  },
  {
    id: 2,
    country: "germany",
    city: "Nürnberg",
    street: "Nelson-Mandela-Platz",
    buildingNo: "18",
    postCode: "90459",

  },
];

const sectors = [{
    id: 1,
    floor: "2",
    sectorNo: "6",
    location: {
      id: 1,
      country: "germany",
      city: "Bad Homburg",
      street: "Marienbader Pl.",
      buildingNo: "1",
      postCode: "61348",
      links: []
    }
  },
  {
    id: 2,
    floor: "1",
    sectorNo: "6",
    location: {
      id: 1,
      country: "germany",
      city: "Bad Homburg",
      street: "Marienbader Pl.",
      buildingNo: "1",
      postCode: "61348",
      links: []
    }
  },
  {
    id: 3,
    floor: "1",
    sectorNo: "5",
    location: {
      id: 1,
      country: "germany",
      city: "Bad Homburg",
      street: "Marienbader Pl.",
      buildingNo: "1",
      postCode: "61348",
      links: []
    }
  }, {
    id: 4,
    floor: "14",
    sectorNo: "555",
    location: {
      id: 2,
      country: "germany",
      city: "Bad Homburg",
      street: "Marienbader Pl.",
      buildingNo: "1",
      postCode: "61348",
      links: []
    }
  }, {
    id: 5,
    floor: "13",
    sectorNo: "666",
    location: {
      id: 2,
      country: "germany",
      city: "Bad Homburg",
      street: "Marienbader Pl.",
      buildingNo: "1",
      postCode: "61348",
      links: []
    }
  },
]

const desks = [
  {
      "id": 1,
      "AP_NR": "1.91",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Nicole",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/1",
              "rel": "self"
          }
      ]
  },
  {
      "id": 2,
      "AP_NR": "1.90",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Igor",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/2",
              "rel": "self"
          }
      ]
  },
  {
      "id": 3,
      "AP_NR": "1.89",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Andreas",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/3",
              "rel": "self"
          }
      ]
  },
  {
      "id": 5,
      "AP_NR": "1.92",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Andy",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/5",
              "rel": "self"
          }
      ]
  },
  {
      "id": 6,
      "AP_NR": "1.93",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Jan",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/6",
              "rel": "self"
          }
      ]
  },
  {
      "id": 7,
      "AP_NR": "1.94",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Frank",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/7",
              "rel": "self"
          }
      ]
  },
  {
      "id": 8,
      "AP_NR": "1.95",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Manoj",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/8",
              "rel": "self"
          }
      ]
  },
  {
      "id": 9,
      "AP_NR": "1.96",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/9",
              "rel": "self"
          }
      ]
  },
  {
      "id": 10,
      "AP_NR": "1.97",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Cornelius",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/10",
              "rel": "self"
          }
      ]
  },
  {
      "id": 12,
      "AP_NR": "1.99",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Anna",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/12",
              "rel": "self"
          }
      ]
  },
  {
      "id": 13,
      "AP_NR": "1.100",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Wolfgang",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/13",
              "rel": "self"
          }
      ]
  },
  {
      "id": 14,
      "AP_NR": "1.101",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Florian",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/14",
              "rel": "self"
          }
      ]
  },
  {
      "id": 15,
      "AP_NR": "1.102",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Gunter",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/15",
              "rel": "self"
          }
      ]
  },
  {
      "id": 16,
      "AP_NR": "1.76",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Ürün",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/16",
              "rel": "self"
          }
      ]
  },
  {
      "id": 17,
      "AP_NR": "1.78",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Maik",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/17",
              "rel": "self"
          }
      ]
  },
  {
      "id": 18,
      "AP_NR": "1.77",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Wolfgang",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/18",
              "rel": "self"
          }
      ]
  },
  {
      "id": 21,
      "AP_NR": "1.75",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Karin",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/21",
              "rel": "self"
          }
      ]
  },
  {
      "id": 22,
      "AP_NR": "1.98",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/22",
              "rel": "self"
          }
      ]
  },
  {
      "id": 23,
      "AP_NR": "1.79",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Ronny",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/23",
              "rel": "self"
          }
      ]
  },
  {
      "id": 24,
      "AP_NR": "1.80",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Detlev",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/24",
              "rel": "self"
          }
      ]
  },
  {
      "id": 25,
      "AP_NR": "1.81",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "German",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/25",
              "rel": "self"
          }
      ]
  },
  {
      "id": 26,
      "AP_NR": "1.82",
      "hardware": {
          "id": 1,
          "hasDockingStation": true,
          "dockingStationType": "HP_NEU",
          "hasMouse": true,
          "hasTelephoneConnection": true,
          "hasKeyboard": true,
          "hasMonitor": true,
          "hasLANCable": true
      },
      "workspace": {
          "id": 1,
          "sector": {
              "id": 1,
              "floor": "2",
              "sectorNo": "6",
              "location": {
                  "id": 1,
                  "country": "germany",
                  "city": "Bad Homburg",
                  "street": "Marienbader Pl.",
                  "buildingNo": "1",
                  "postCode": "61348",
                  "links": []
              }
          },
          "group": {
              "id": 1,
              "groupName": "Output & Archiving",
              "groupNumber": "44",
              "groupManager": "Maik Dittrich"
          }
      },
      "deskOwner": "Vincenzo",
      "links": [
          {
              "link": "https://localhost/deskable/webapi/desks/26",
              "rel": "self"
          }
      ]
  }
]

const reservations = [
    {
        "id": 7,
        "user": {
            "id": 1,
            "firstname": " Emre",
            "lastname": "Cumbus",
            "email": "Emre.Cumbus@firstdata.com",
            "title": "Technical Trainer",
            "credentials": {
                "username": "f70e9z0",
                "password": "43eb567ee8095b25bba56292ef09c6e039493c87f88baf84890140058beb116c",
                "authority": "POSTRESERVATION",
                "links": []
            },
            "group": {
                "id": 2,
                "groupName": " Output & Archiving",
                "groupNumber": "FDD414",
                "groupManager": "CN=Dittrich, Maik,CN=Users,DC=gzs,DC=de"
            },
            "links": []
        },
        "desk": {
            "id": 1,
            "AP_NR": "1.91",
            "hardware": {
                "id": 1,
                "hasDockingStation": true,
                "dockingStationType": "HP_NEU",
                "hasMouse": true,
                "hasTelephoneConnection": true,
                "hasKeyboard": true,
                "hasMonitor": true,
                "hasLANCable": true
            },
            "workspace": {
                "id": 1,
                "sector": {
                    "id": 1,
                    "floor": "2",
                    "sectorNo": "6",
                    "location": {
                        "id": 1,
                        "country": "germany",
                        "city": "Bad Homburg",
                        "street": "Marienbader Pl.",
                        "buildingNo": "1",
                        "postCode": "61348",
                        "links": []
                    }
                },
                "group": {
                    "id": 1,
                    "groupName": "Output & Archiving",
                    "groupNumber": "44",
                    "groupManager": "Maik Dittrich"
                }
            },
            "deskOwner": "Nicole",
            "links": []
        },
        "from": "2019-11-20",
        "to": "2019-11-20",
        "links": []
    },
    {
        "id": 8,
        "user": {
            "id": 1,
            "firstname": " Emre",
            "lastname": "Cumbus",
            "email": "Emre.Cumbus@firstdata.com",
            "title": "Technical Trainer",
            "credentials": {
                "username": "f70e9z0",
                "password": "43eb567ee8095b25bba56292ef09c6e039493c87f88baf84890140058beb116c",
                "authority": "POSTRESERVATION",
                "links": []
            },
            "group": {
                "id": 2,
                "groupName": " Output & Archiving",
                "groupNumber": "FDD414",
                "groupManager": "CN=Dittrich, Maik,CN=Users,DC=gzs,DC=de"
            },
            "links": []
        },
        "desk": {
            "id": 2,
            "AP_NR": "1.90",
            "hardware": {
                "id": 1,
                "hasDockingStation": true,
                "dockingStationType": "HP_NEU",
                "hasMouse": true,
                "hasTelephoneConnection": true,
                "hasKeyboard": true,
                "hasMonitor": true,
                "hasLANCable": true
            },
            "workspace": {
                "id": 1,
                "sector": {
                    "id": 1,
                    "floor": "2",
                    "sectorNo": "6",
                    "location": {
                        "id": 1,
                        "country": "germany",
                        "city": "Bad Homburg",
                        "street": "Marienbader Pl.",
                        "buildingNo": "1",
                        "postCode": "61348",
                        "links": []
                    }
                },
                "group": {
                    "id": 1,
                    "groupName": "Output & Archiving",
                    "groupNumber": "44",
                    "groupManager": "Maik Dittrich"
                }
            },
            "deskOwner": "Igor",
            "links": []
        },
        "from": "2019-11-21",
        "to": "2019-11-21",
        "links": []
    },
    {
        "id": 9,
        "user": {
            "id": 2,
            "firstname": "Azida",
            "lastname": "Yambaeva",
            "email": "azida.yambaeva@firstdata.com",
            "title": "Trainee",
            "credentials": {
                "username": "azida",
                "password": "1234",
                "authority": "ADMIN",
                "links": []
            },
            "lastLogin": "2019-09-24",
            "group": {
                "id": 1,
                "groupName": "Output & Archiving",
                "groupNumber": "44",
                "groupManager": "Maik Dittrich"
            },
            "links": []
        },
        "desk": {
            "id": 3,
            "AP_NR": "1.89",
            "hardware": {
                "id": 1,
                "hasDockingStation": true,
                "dockingStationType": "HP_NEU",
                "hasMouse": true,
                "hasTelephoneConnection": true,
                "hasKeyboard": true,
                "hasMonitor": true,
                "hasLANCable": true
            },
            "workspace": {
                "id": 1,
                "sector": {
                    "id": 1,
                    "floor": "2",
                    "sectorNo": "6",
                    "location": {
                        "id": 1,
                        "country": "germany",
                        "city": "Bad Homburg",
                        "street": "Marienbader Pl.",
                        "buildingNo": "1",
                        "postCode": "61348",
                        "links": []
                    }
                },
                "group": {
                    "id": 1,
                    "groupName": "Output & Archiving",
                    "groupNumber": "44",
                    "groupManager": "Maik Dittrich"
                }
            },
            "deskOwner": "Andreas",
            "links": []
        },
        "from": "2019-11-20",
        "to": "2019-11-20",
        "links": []
    },
    {
        "id": 11,
        "user": {
            "id": 2,
            "firstname": "Azida",
            "lastname": "Yambaeva",
            "email": "azida.yambaeva@firstdata.com",
            "title": "Trainee",
            "credentials": {
                "username": "azida",
                "password": "1234",
                "authority": "ADMIN",
                "links": []
            },
            "lastLogin": "2019-09-24",
            "group": {
                "id": 1,
                "groupName": "Output & Archiving",
                "groupNumber": "44",
                "groupManager": "Maik Dittrich"
            },
            "links": []
        },
        "desk": {
            "id": 5,
            "AP_NR": "1.92",
            "hardware": {
                "id": 1,
                "hasDockingStation": true,
                "dockingStationType": "HP_NEU",
                "hasMouse": true,
                "hasTelephoneConnection": true,
                "hasKeyboard": true,
                "hasMonitor": true,
                "hasLANCable": true
            },
            "workspace": {
                "id": 1,
                "sector": {
                    "id": 1,
                    "floor": "2",
                    "sectorNo": "6",
                    "location": {
                        "id": 1,
                        "country": "germany",
                        "city": "Bad Homburg",
                        "street": "Marienbader Pl.",
                        "buildingNo": "1",
                        "postCode": "61348",
                        "links": []
                    }
                },
                "group": {
                    "id": 1,
                    "groupName": "Output & Archiving",
                    "groupNumber": "44",
                    "groupManager": "Maik Dittrich"
                }
            },
            "deskOwner": "Andy",
            "links": []
        },
        "from": "2019-11-23",
        "to": "2019-11-23",
        "links": []
    },
    {
        "id": 12,
        "user": {
            "id": 6,
            "firstname": "Sergej",
            "lastname": "Kukshausen",
            "email": "Sergej.Kukshausen@firstdata.com",
            "title": "CWR-Project Support",
            "credentials": {
                "username": "sergej",
                "password": "1234",
                "authority": "ADMIN",
                "links": []
            },
            "lastLogin": "2019-11-02",
            "group": {
                "id": 1,
                "groupName": "Output & Archiving",
                "groupNumber": "44",
                "groupManager": "Maik Dittrich"
            },
            "links": []
        },
        "desk": {
            "id": 6,
            "AP_NR": "1.93",
            "hardware": {
                "id": 1,
                "hasDockingStation": true,
                "dockingStationType": "HP_NEU",
                "hasMouse": true,
                "hasTelephoneConnection": true,
                "hasKeyboard": true,
                "hasMonitor": true,
                "hasLANCable": true
            },
            "workspace": {
                "id": 1,
                "sector": {
                    "id": 1,
                    "floor": "2",
                    "sectorNo": "6",
                    "location": {
                        "id": 1,
                        "country": "germany",
                        "city": "Bad Homburg",
                        "street": "Marienbader Pl.",
                        "buildingNo": "1",
                        "postCode": "61348",
                        "links": []
                    }
                },
                "group": {
                    "id": 1,
                    "groupName": "Output & Archiving",
                    "groupNumber": "44",
                    "groupManager": "Maik Dittrich"
                }
            },
            "deskOwner": "Jan",
            "links": []
        },
        "from": "2019-11-20",
        "to": "2019-11-20",
        "links": []
    },
    {
        "id": 13,
        "user": {
            "id": 7,
            "firstname": "Tobias",
            "lastname": "Grundlach",
            "email": "Tobias.Gundlach@firstdata.com",
            "title": "CWR-Application Analyst",
            "credentials": {
                "username": "tobias",
                "password": "1234",
                "authority": "ADMIN",
                "links": []
            },
            "lastLogin": "2019-11-04",
            "group": {
                "id": 1,
                "groupName": "Output & Archiving",
                "groupNumber": "44",
                "groupManager": "Maik Dittrich"
            },
            "links": []
        },
        "desk": {
            "id": 7,
            "AP_NR": "1.94",
            "hardware": {
                "id": 1,
                "hasDockingStation": true,
                "dockingStationType": "HP_NEU",
                "hasMouse": true,
                "hasTelephoneConnection": true,
                "hasKeyboard": true,
                "hasMonitor": true,
                "hasLANCable": true
            },
            "workspace": {
                "id": 1,
                "sector": {
                    "id": 1,
                    "floor": "2",
                    "sectorNo": "6",
                    "location": {
                        "id": 1,
                        "country": "germany",
                        "city": "Bad Homburg",
                        "street": "Marienbader Pl.",
                        "buildingNo": "1",
                        "postCode": "61348",
                        "links": []
                    }
                },
                "group": {
                    "id": 1,
                    "groupName": "Output & Archiving",
                    "groupNumber": "44",
                    "groupManager": "Maik Dittrich"
                }
            },
            "deskOwner": "Frank",
            "links": []
        },
        "from": "2019-11-20",
        "to": "2019-11-20",
        "links": []
    }
]

const httpHeaders = {
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*',
};

server.all('/', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next()
});

server.get("/api/locations", (req, res) => {
  console.log('/locations/ is called');
  res.set(httpHeaders);
  res.json(locations);
});

server.get("/api/locations/:id/sectors", (req, res) => {
  const locationId = req.params.id;
  console.log(`/api/locations/${locationId}/sectors is called`);
  const selectedSectors = sectors.filter(_sector => _sector.location.id == locationId);
  res.set(httpHeaders);
  res.json(selectedSectors);
});

server.get("/api/locations/:id/desks", (req, res) => {
  const locationId = req.params.id;
  console.log(`/api/locations/${locationId}/desks is called`);
  const selectedDesks = desks.filter(_desk => _desk.workspace.sector.location.id == locationId);
  res.set(httpHeaders);
  res.json(selectedDesks);
});

server.get("/api/locations/:id/reservations", (req, res) => {
  const locationId = req.params.id;
  console.log(`/api/locations/${locationId}/reservations is called`);
  const selectedReservations = reservations
    .filter(_reservation => _reservation.desk.workspace.sector.location.id == locationId);
  res.set(httpHeaders);
  res.json(selectedReservations);
});

server.post("/api/reservations", (req, res, next) => {
  console.log(`post on /api/reservations is called`);

  const newReservation = req.body;
  console.log(newReservation);
  //console.log(reservations);
  reservations.push(newReservation);
  //console.log(reservations);
  res.json(newReservation);
  /*res.header("Access-Control-Allow-Origin", "http://localhost:4200"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "http://localhost:4200, X-Requested-With, Content-Type, Accept");
  res.json(newReservation);*/
});


server.listen(port, () => {
  console.log(`Server listening at ${port}`);
});
