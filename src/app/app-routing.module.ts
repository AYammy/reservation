import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { AuthGuardService } from './guards/auth-guard.service';
import { TabsComponent } from './components/main-content/tabs/tabs.component';

const routes: Routes = [
  { path: '', redirectTo: '/desks', pathMatch: 'full', canActivate: [AuthGuardService] },
  { path: 'desks', component: TabsComponent, canActivate: [AuthGuardService] },
  { path: 'login', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})
export class AppRoutingModule { }
