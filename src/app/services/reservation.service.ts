import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { Reservation } from '../models/reservation';
import { Desk } from '../models/desk';
import { User } from '../models/user';


@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  private reservationsUrl = environment.apiUrl + '/reservations';

  constructor(private http: HttpClient) { }

  /** GET reservations with HttpClient. */
  getReservations(): Observable<Reservation[]> {
    return this.http.get<Reservation[]>(this.reservationsUrl);
  }

  //TODO: change url http://localhost:8080/deskable/webapi/reservation/desks/{deskId} 
  getReservationsByLocationId(locationId: number): Observable<Reservation[]> {
    let url = `${environment.apiUrl}/locations/${locationId}/reservations`;
    return this.http.get<Reservation[]>(url);
  }


  postReservation(desk: Desk, date: Date): Observable<Reservation> {
    let url = `${environment.apiUrl}/reservations`;
    let reservation = new Reservation();
    reservation.desk = desk;
    reservation.id = Math.random();
    reservation.from = date.toISOString();
    reservation.to = date.toISOString();
    let user = new User();
    user.id = 12;
    user.firstname = 'Azida';
    reservation.user = user;
    console.log('Reservation to be pushed');
    console.log(reservation);
    return this.http.post<Reservation>(url, reservation, environment.httpOptions).pipe();
  }
}
