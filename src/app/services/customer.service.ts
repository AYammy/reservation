import { Injectable } from '@angular/core';

const TOKEN = 'TOKEN';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  setToken(token: string): void {
    console.log('Setting TOKEN to ' + token);
    localStorage.setItem(TOKEN, token);
  }

  isLogged() {
    return localStorage.getItem(TOKEN) != null;
  }

  removeToken(): void {
    console.log('Removing TOKEN.');
    localStorage.removeItem(TOKEN);
  }
}
