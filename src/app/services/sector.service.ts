import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { Sector } from '../models/sector';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class SectorService {

  constructor(private http: HttpClient) { }

  /**  GET sectors by location.*/
  getSectorsByLocation(locationId: number): Observable<Sector[]> {
    var url = `${environment.apiUrl}/locations/${locationId}/sectors`;
    return this.http.get<Sector[]>(url);
  }

}
