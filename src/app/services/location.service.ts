import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { Location } from '../models/location';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  private locationsUrl = environment.apiUrl + '/locations';

  constructor(private http: HttpClient) { }

  /** GET locations with HttpClient. */
  getLocations(): Observable<Location[]> {
    return this.http.get<Location[]>(this.locationsUrl);
  }

  /** GET Location by id. Will 404 if id not found */
  getLocation(id: number): Observable<Location> {
    let url = `${environment.apiUrl}/locations/${id}`;
    //const url = `${this.desksUrl}/${id}`;
    return this.http.get<Location>(url);
  }
}
