import { Injectable } from '@angular/core';
import { MatSelectChange } from '@angular/material';
import { Subject } from 'rxjs';
import { Location } from '../models/location';
import { Sector } from '../models/sector';
/** FORDATEimport { Datepicker } from '../models/date'*/

@Injectable({
  providedIn: 'root'
})
export class SubjectsService {


  //FIXME Do we still need this?
  private locationSubject = new Subject<Location>();
  private sectorsSubject = new Subject<Sector>();
  private dateSubject = new Subject<Date>();

  constructor() { }

  getLocationSubject() {
    return this.locationSubject;
  }

  getSectorsSubject() {
    return this.sectorsSubject;
  }

  getDateSubject() {
    return this.dateSubject;
  }
}
