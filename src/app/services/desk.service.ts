// The deskService could get desk data from anywhere—a web service, local storage, or a mock data source.
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { Desk } from '../models/desk';
import { Location } from '../models/location';
import { environment } from '../../environments/environment';
// import { catchError, map, tap } from 'rxjs/operators';
// import { MessageService } from './message.service';

@Injectable({ providedIn: 'root' })

export class DeskService {

  constructor(private http: HttpClient) { }

  getDesksByLocationId(locationId: number): Observable<Desk[]> {
    let url = `${environment.apiUrl}/locations/${locationId}/desks`;
    return this.http.get<Desk[]>(url);
  }

  /** GET desk by id. Will 404 if id not found */
  getDesk(id: number): Observable<Desk> {
    let url = `${environment.apiUrl}/desks/${id}`;
    //const url = `${this.desksUrl}/${id}`;
    return this.http.get<Desk>(url);
  }
}
