import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';
import { Workspace } from '../models/workspace';

@Injectable({
  providedIn: 'root'
})
export class WorkspaceService {

  private workspacesUrl = environment.apiUrl + '/workspaces';
  

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient // inject

  ) { }

  /** GET workspaces with HttpClient. */
  getWorkspaces(): Observable<Workspace[]> {
    return this.http.get<Workspace[]>(this.workspacesUrl);
  }
}
