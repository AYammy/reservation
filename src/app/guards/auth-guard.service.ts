import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { CustomerService } from '../services/customer.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private router: Router, private customerService: CustomerService) { }

  public login(username: string, password: string): void {
    if (username === 'asdf56' && password === '1234') {
      console.debug('AuthGuardService: login successfull.');
      this.customerService.setToken(username + password);
    }
  }

  canActivate(
    route: import('@angular/router').ActivatedRouteSnapshot,
    state: import('@angular/router').RouterStateSnapshot):
    boolean | import('@angular/router').UrlTree | import('rxjs').Observable<boolean
    | import('@angular/router').UrlTree> | Promise<boolean | import('@angular/router').UrlTree> {
    if (this.customerService.isLogged()) {
      return true;
    }

    this.router.navigate(['/login']);
    return false;
  }
}
